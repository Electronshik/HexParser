#pragma once
#ifndef __LOGGER_H__
#define __LOGGER_H__

#include <stdint.h>
#include <iostream>


enum class ErrorCode
{
	OK,
	ERROR
};

class IFlashWriter
{
	public:
		const uint32_t FLASH_PAGE_SIZE;		
		virtual ErrorCode Write (uint32_t Address, uint8_t *Data, uint32_t Size) = 0;
		virtual ~IFlashWriter () {}
	protected:
		IFlashWriter (uint32_t FlashPageSize) : FLASH_PAGE_SIZE (FlashPageSize) {}
};

class HexParser
{
	public:
		HexParser (IFlashWriter &FlashWriter) : FlashWriter(FlashWriter)
		{
			PageBuffer = new uint8_t[FlashWriter.FLASH_PAGE_SIZE];
		}
		template<typename T>
		ErrorCode ProcessString (T &String, const uint8_t Length)
		{
			HexData_t HexData;
			AsciiToHex (String, Length);
			HexData.Size = String[1] | (String[0] << 4);
			HexData.Address = String[5] | (String[4] << 4) | (String[3] << 8) | (String[2] << 12);
			HexData.Type = String[7] | (String[6] << 4);
			uint8_t HexDataCalculatedCrc = HexData.Size + (uint8_t)HexData.Address + (uint8_t)(HexData.Address >> 8) + HexData.Type;
			printf("Size: %x Addr: %x Type: %x Calc CRC: %x\n", HexData.Size, HexData.Address, HexData.Type, HexDataCalculatedCrc);
			if (HexData.Type == 0x00)
			{
				uint8_t Data[HexData.Size];
				uint8_t *DataPtr = Data;
				HexData.Size = HexData.Size << 1;
				for(int i = 0; i < HexData.Size; i += 2)
				{
					*DataPtr = String[i+9] | (String[i+8] << 4);
					HexDataCalculatedCrc += *DataPtr;
					DataPtr++;
				}
				HexData.Crc = String[9+HexData.Size] | (String[8+HexData.Size] << 4);
				HexDataCalculatedCrc = 256 - HexDataCalculatedCrc;
				if (HexDataCalculatedCrc == HexData.Crc)
				{
					//std::cout << "Crc is ok" << std::endl;
					return ErrorCode::OK;
				}
				else
				{
					//std::cout << "Crc is not ok!" << std::endl;
					return ErrorCode::ERROR;
				}
			}
			return ErrorCode::OK;
		}

	private:
		IFlashWriter &FlashWriter;
		uint8_t *PageBuffer;
		struct HexData_t
		{
			uint8_t Size;
			uint8_t Type;
			uint16_t Address;
			uint8_t Crc;
		};
		template<typename T>
		void AsciiToHex (T &Buffer, const uint8_t Length)
		{
			for(uint8_t i = 0; i < Length; i++)
			{
				if ((Buffer[i] >= '0') && (Buffer[i] <= '9'))
				{
					Buffer[i] -= 0x30;
				}
				else
				{
					Buffer[i] -= 0x37;
				}
			}
		}
};

#endif
