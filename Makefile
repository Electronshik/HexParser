export CPPUTEST_HOME=./cpputest

ifeq ($(OS),Windows_NT)
	CXX := "C:\MinGW\bin\g++.exe"
	OUT_MAIN := main.exe
	OUT_TEST := test_main.exe
else
	CXX := g++
	OUT_MAIN := main
	OUT_TEST := test_main
endif

BUILD_DIR := build

CPPFLAGS += -g
CPPFLAGS += -I./
CPPFLAGS += -I./etl/include
CPPFLAGS += -I$(CPPUTEST_HOME)/include -std=c++17
LD_LIBRARIES = -L$(CPPUTEST_HOME)/cpputest_build/lib -lCppUTest #-lstdc++
#CXXFLAGS += -include $(CPPUTEST_HOME)/include/CppUTest/MemoryLeakDetectorNewMacros.h
#CFLAGS += -include $(CPPUTEST_HOME)/include/CppUTest/MemoryLeakDetectorMallocMacros.h

MAIN_SRC := main.cpp HexParser.cpp
MAIN_OBJS := $(MAIN_SRC:%.cpp=$(BUILD_DIR)/%.o)

TEST_SRC := test_main.cpp test.cpp HexParser.cpp
TEST_OBJS := $(TEST_SRC:%.cpp=$(BUILD_DIR)/%.o)

all: main test_main

main: $(MAIN_OBJS)
	$(CXX) -g -o $(BUILD_DIR)/$(OUT_MAIN) $(MAIN_OBJS)

test_main: $(TEST_OBJS)
	$(CXX) -g -o $(BUILD_DIR)/$(OUT_TEST) $(TEST_OBJS) $(LD_LIBRARIES)

clean:
	rm -f $(BUILD_DIR)/*.o $(BUILD_DIR)/$(OUT_MAIN) $(BUILD_DIR)/$(OUT_TEST)

$(BUILD_DIR)/%.o: %.cpp
	@mkdir -p $(dir $@) 2> NUL || echo off
	$(CXX) $(CPPFLAGS) -c $< -o $@
