#!/bin/sh
echo "Building main and tests..."
git submodule init
git submodule update
cd cpputest/cpputest_build
autoreconf .. -i
../configure
make
cd ../../
make all
echo "Running test_main.."
./build/test_main
echo "Running main.."
./build/main
