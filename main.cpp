#include "HexParser.hpp"
#include <iostream>
//#include "etl_profile.h"
#include "etl/string.h"

using namespace std;

class Flash : public IFlashWriter
{
	public:
		Flash (uint32_t FlashPageSize) : IFlashWriter (FlashPageSize) {}
		virtual ErrorCode Write (uint32_t Address, uint8_t *Data, uint32_t Size) override
		{
			cout << "Flash Write" << endl;
			return ErrorCode::OK;
		}
};

int main(int ac, char** av)
{

	etl::string<8> STR = "heiil!";
	printf("%s\n", STR.c_str());

	Flash MemWriter(880);
	printf("Page size: %d\n", MemWriter.FLASH_PAGE_SIZE);
	HexParser Parser (MemWriter);
	//uint8_t str[] = "10005000C302000825560008C3020008C3020008B6";
	etl::string<43> str = "10005000C302000825560008C3020008C3020008B6";
	cout << "1: " << str[0] << " 2: " << str[4] << endl;
	if (Parser.ProcessString (str, 43) == ErrorCode::OK)
	{
		cout << "Parser OK " << endl;
	}
	else
	{
		cout << "Parser ERROR " << endl;
	}

}
