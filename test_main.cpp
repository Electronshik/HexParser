#include "HexParser.hpp"
#include <iostream>
//#include "etl_profile.h"
#include "etl/string.h"

#include "CppUTest/CommandLineTestRunner.h"

using namespace std;

class Flash : public IFlashWriter
{
	public:
		Flash (uint32_t FlashPageSize) : IFlashWriter (FlashPageSize) {}
		virtual ErrorCode Write (uint32_t Address, uint8_t *Data, uint32_t Size) override
		{
			//cout << "Flash Write" << endl;
			return ErrorCode::OK;
		}
		uint32_t GetPageSize ()
		{
			return FLASH_PAGE_SIZE;
		}
};

int main(int ac, char** av)
{

	etl::string<8> STR = "heiil!";
	printf("%s\n", STR.c_str());

	Flash MemWriter(880);
	printf("Page size: %d\n", MemWriter.GetPageSize());
	HexParser Parser (MemWriter);
	uint8_t str[] = "10005000C302000825560008C3020008C3020008B6";
	//cout << str << endl;
	if (Parser.ProcessString (str, 42) == ErrorCode::OK)
	{
		cout << "Parser OK " << str << endl;
	}
	else
	{
		cout << "Parser ERROR " << str << endl;
	}

	return CommandLineTestRunner::RunAllTests(ac, av);

}
